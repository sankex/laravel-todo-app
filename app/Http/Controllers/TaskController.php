<?php

namespace App\Http\Controllers;
use App\Task;
use Illuminate\Http\Request;

class TaskController extends Controller
{
    //Gets all unarchived tasks
    public function index()
    {
        return Task::where('archive', 0)->orderBy('id', 'desc')->get();
    }

    //Get all tasks tagged as archived
    public function archived()
    {
        return Task::where('archive', 1)->orderBy('id', 'desc')->get();
    }

    //Validates the form input before creating a task record
    public function store(Request $request)
    {
        $this->validate($request, [
            'body' => 'required|max:500'
        ]);

        return Task::create(['body' => request('body')]);
    }

    //Updated the task record with user-submitted edits
    public function edit(Request $request)
    {
        $this->validate($request, [
            'body' => 'required|max:500'
        ]);

        $task = Task::findOrFail($request->id);
        $task->body = $request->body;
        $task->save();
    }

    //Toggles the archiev state of a task record
    public function archive($id)
    {
        $task = Task::findOrFail($id);
        $task->archive = ! $task->atchive;
        $task->save();
    }

    //Removes a task record
    public function delete($id)
    {
        $task = Task::findOrFail($id);
        $task->delete();
    }
}
